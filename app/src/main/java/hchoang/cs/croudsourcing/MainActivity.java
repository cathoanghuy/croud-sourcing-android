package hchoang.cs.croudsourcing;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import layout.LoginFragment;
import layout.NewUserFragment;

public class MainActivity extends AppCompatActivity implements
        LoginFragment.OnFragmentInteractionListener,
        NewUserFragment.OnFragmentInteractionListener {
    private LoginFragment loginFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loginFragment = new LoginFragment();

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_container, loginFragment).commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void newUser(View view) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        NewUserFragment fragment = NewUserFragment.newInstance();
        fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).commit();
    }
}
