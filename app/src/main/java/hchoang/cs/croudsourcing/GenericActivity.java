package hchoang.cs.croudsourcing;

import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import layout.BaseFragment;
import layout.DiscussionFragment;
import layout.NewDiscussionFragment;
import layout.ServiceDetailFragment;
import layout.ServiceFragment;
import layout.UserDetailFragment;
import layout.WriteReviewFragment;
import models.Category;
import models.Service;

public class GenericActivity extends AppCompatActivity implements DiscussionFragment.OnFragmentInteractionListener,
        ServiceFragment.OnFragmentInteractionListener,
        ServiceDetailFragment.OnFragmentInteractionListener,
        UserDetailFragment.OnFragmentInteractionListener,
        WriteReviewFragment.OnFragmentInteractionListener,
        NewDiscussionFragment.OnFragmentInteractionListener {

    private BaseFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discussion);
        String layout = getIntent().getStringExtra("layout");
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (layout == null) {
            return;
        }
        if (layout.equals(getString(R.string.discussion_layout))) {
            fragment = new DiscussionFragment();
            fragmentManager.beginTransaction().add(R.id.generic_fragment_container, fragment).commit();
        } else if (layout.equals(getString(R.string.service_layout))) {
            Category category = (Category) getIntent().getSerializableExtra("category");
            fragment = ServiceFragment.newInstance(category);
            fragmentManager.beginTransaction().add(R.id.generic_fragment_container, fragment).commit();
        } else if (layout.equals(getString(R.string.service_detail_layout))) {
            Service service = (Service) getIntent().getSerializableExtra("service");
            fragment = ServiceDetailFragment.newInstance(service);
            fragmentManager.beginTransaction().add(R.id.generic_fragment_container, fragment).commit();
        } else if (layout.equals(getString(R.string.user_profile_layout))) {
            fragment = UserDetailFragment.newInstance();
            fragmentManager.beginTransaction().add(R.id.generic_fragment_container, fragment).commit();
        } else if (layout.equals(getString(R.string.write_review_layout))) {
            Service service = (Service) getIntent().getSerializableExtra("service");
            fragment = WriteReviewFragment.newInstance(service);
            fragmentManager.beginTransaction().add(R.id.generic_fragment_container, fragment).commit();
        } else if (layout.equals(getString(R.string.new_discussion_layout))) {
            fragment = NewDiscussionFragment.newInstance();
            fragmentManager.beginTransaction().add(R.id.generic_fragment_container, fragment).commit();
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

}
