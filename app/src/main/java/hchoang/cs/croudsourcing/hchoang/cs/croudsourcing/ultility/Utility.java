package hchoang.cs.croudsourcing.hchoang.cs.croudsourcing.ultility;

import com.loopj.android.http.RequestParams;

import java.io.Console;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by hchoang on 16/9/16.
 */
public class Utility {
    private static Pattern pattern;
    private static Matcher matcher;

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public static String APP_TOKEN;
    public static String SERVER;

    public static boolean validateEmail(String email)
    {
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);

        return matcher.matches();
    }

    public static boolean isNotNull(String text)
    {
        return text != null && text.trim().length() > 0;
    }

    public static void sendRequest(Map<String, String> params) {
        RequestParams requestParams = new RequestParams();
        for (Entry<String, String> param : params.entrySet()) {
            requestParams.put(param.getKey(), param.getValue());
        }
    }
    
    public static String MD5(String text) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(text.getBytes());
            byte[] digest = md.digest();
            StringBuffer sb = new StringBuffer();
            for (byte b : digest) {
                sb.append(String.format("%02x", b & 0xff));
            }
            
            return sb.toString();
        } catch (NoSuchAlgorithmException ex) {
            System.out.println(ex.getMessage());
        }
        return text;
    }
}
