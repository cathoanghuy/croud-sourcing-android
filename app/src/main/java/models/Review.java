package models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by hchoang on 10/10/16.
 */

public class Review {
    private int id;
    private String title;
    private String description;
    private Reviewer reviewer;

    public Review (JSONObject object) {
        try {
            this.id = object.getInt("review_id");
            this.title = (object.getString("review_title") != null) ? object.getString("review_title") : "";
            this.description = (object.getString("review_desc") != null) ? object.getString("review_desc") : "";
            this.reviewer = new Reviewer(object.getJSONObject("author"));
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public static ArrayList<Review> fromJSON(JSONArray jsonArray) {
        ArrayList<Review> reviews = new ArrayList<Review>();
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                reviews.add(new Review(jsonArray.getJSONObject(i)));
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        return reviews;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Reviewer getReviewer() {
        return reviewer;
    }
}
