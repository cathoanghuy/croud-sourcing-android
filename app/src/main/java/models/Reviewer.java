package models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by hchoang on 22/09/16.
 */

public class Reviewer implements Serializable {
    private int id;
    private String firstName;
    private String lastName;
    private String email;
    private String reviewerCode;
    private String reviewerPosition;
    private int numberOfReview;
    private int reviewerPoint;
    private boolean status;

    public Reviewer(int id, String firstName, String lastName, String email, String reviewerCode, String reviewerPosition, int numberOfReview, int reviewerPoint, boolean status) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.reviewerCode = reviewerCode;
        this.reviewerPosition = reviewerPosition;
        this.numberOfReview = numberOfReview;
        this.reviewerPoint = reviewerPoint;
        this.status = status;
    }

    public Reviewer(JSONObject jsonObject) {
        try {
            this.id = jsonObject.getInt("reviewer_id");
            this.firstName = jsonObject.getString("reviewer_first_name");
            this.lastName = jsonObject.getString("reviewer_last_name");
            this.email = jsonObject.getString("reviewer_email");
            this.reviewerPosition = jsonObject.getString("reviewer_position");
            this.numberOfReview = jsonObject.getInt("reviewer_no_of_reviews");
            this.reviewerPoint = jsonObject.getInt("reviewer_points");
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }
}
