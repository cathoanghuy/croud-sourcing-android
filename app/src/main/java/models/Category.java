package models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by hchoang on 22/09/16.
 */

public class Category implements Serializable {
    private int id;
    private String name;
    private ArrayList<Service> services;

    public Category(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Category(JSONObject object) {
        try {
            this.id = object.getInt("cat_id");
            this.name = object.getString("cat_name");
            if (object.getJSONArray("services") != null) {
                this.services = Service.fromJSON(object.getJSONArray("services"));
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public static ArrayList<Category> fromJSON(JSONArray jsonArray) {
        ArrayList<Category> categories = new ArrayList<Category>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                categories.add(new Category(jsonArray.getJSONObject(i)));
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }
        return categories;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Service> getServices() {
        return services;
    }
}
