package models.listener;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import hchoang.cs.croudsourcing.GenericActivity;
import hchoang.cs.croudsourcing.R;
import models.Service;

/**
 * Created by hchoang on 10/10/16.
 */

public class ViewServiceListener implements View.OnClickListener {

    private Context context;

    public ViewServiceListener(Context context) {
        this.context = context;
    }

    @Override
    public void onClick(View view) {
        Service service = (Service) view.getTag();
        // Access the row position here to get the correct data item
        Intent intent = new Intent(this.context, GenericActivity.class);
        intent.putExtra("layout", context.getString(R.string.service_detail_layout));
        intent.putExtra("service", service);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }
}
