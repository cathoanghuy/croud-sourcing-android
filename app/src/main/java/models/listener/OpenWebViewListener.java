package models.listener;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

/**
 * Created by hchoang on 11/10/16.
 */

public class OpenWebViewListener implements View.OnClickListener {
    private String url;
    private Activity context;
    public OpenWebViewListener(Activity context, String url) {
        this.context = context;
        this.url = url;
    }

    @Override
    public void onClick(View v) {
        Log.i("URL", "onClick: " + url);
        WebView webView = new WebView(context);
        webView.getSettings().setJavaScriptEnabled(true); // enable javascript

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                Toast.makeText(context, error.getDescription(), Toast.LENGTH_SHORT).show();
            }
        });

        webView.loadUrl(url);
        context.setContentView(webView);
    }
}
