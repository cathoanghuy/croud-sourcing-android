package models.listener;

import android.app.Activity;
import android.view.View;

/**
 * Created by hchoang on 12/10/16.
 */

public class CloseCurrentActivityListener implements View.OnClickListener {
    private Activity activity;
    public CloseCurrentActivityListener(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void onClick(View v) {
        activity.finish();
    }
}
