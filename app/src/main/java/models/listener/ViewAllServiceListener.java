package models.listener;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import hchoang.cs.croudsourcing.GenericActivity;
import hchoang.cs.croudsourcing.R;
import models.Category;

/**
 * Created by hchoang on 7/10/16.
 */

public class ViewAllServiceListener implements View.OnClickListener {

    private Category category;
    private Context context;

    public ViewAllServiceListener(Category category, Context context) {
        this.category = category;
        this.context = context;
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(context, GenericActivity.class);
        intent.putExtra("category", category);
        intent.putExtra("layout", context.getString(R.string.service_layout));
        context.startActivity(intent);
    }
}
