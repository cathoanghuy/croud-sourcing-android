package models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by hchoang on 27/09/16.
 */

public class Service implements Serializable{
    private int id;
    private String name;
    private double editorRatings;
    private String description;
    private boolean serviceTrial;
    private boolean mobileApp;
    private double price;
    private String serviceLink;
    private boolean reviewable;

    public Service (JSONObject object) {
        try {
            this.id = object.getInt("serv_id");
            this.name = object.getString("serv_name");
            this.editorRatings = object.getDouble("editor_ratings");
            if (object.has("serv_description")) {
                this.description = object.getString("serv_description");
            }

            if (object.has("serv_trial")) {
                this.serviceTrial = object.getInt("serv_trial") == 1;
            }
            if (object.has("serv_mobile_app")) {
                this.mobileApp = object.getInt("serv_mobile_app") == 1;
            }

            if (object.has("serv_starting_price")) {
                this.price = object.getDouble("serv_starting_price");
            }

            if (object.has("serv_link")) {
                this.serviceLink = object.getString("serv_link");
            } else {
                this.serviceLink = null;
            }

            if (object.has("is_reviewable")) {
                this.reviewable = object.getBoolean("is_reviewable");
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getEditorRatings() {
        return editorRatings;
    }

    public String getDescription() {
        return description;
    }

    public boolean isServiceTrial() {
        return serviceTrial;
    }

    public boolean hasMobileApp() {
        return mobileApp;
    }

    public double getPrice() {
        return price;
    }

    public String getServiceLink() {
        return serviceLink;
    }

    public static ArrayList<Service> fromJSON(JSONArray jsonArray) {
        ArrayList<Service> services = new ArrayList<>();
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                services.add(new Service(jsonArray.getJSONObject(i)));
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        return services;
    }

    public boolean isReviewable() {
        return reviewable;
    }
}
