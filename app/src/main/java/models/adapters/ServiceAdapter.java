package models.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

import hchoang.cs.croudsourcing.R;
import models.Service;
import models.listener.ViewServiceListener;

/**
 * Created by hchoang on 30/09/16.
 */

public class ServiceAdapter extends ArrayAdapter<Service> {
    public ServiceAdapter(Context context, ArrayList<Service> services) {
        super(context, 0, services);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Service service =  getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_service, parent, false);
        }

        TextView serviceName = (TextView) convertView.findViewById(R.id.reviewTitle_TextView);
        serviceName.setText(service.getName());

        convertView.setTag(service);

        RatingBar ratingBar = (RatingBar) convertView.findViewById(R.id.serviceRating_ratingBar);
        ratingBar.setRating((float) service.getEditorRatings());

        convertView.setOnClickListener(new ViewServiceListener(getContext()));

        return convertView;
    }
}
