package models.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import hchoang.cs.croudsourcing.R;
import models.Discussion;
import models.Reviewer;

/**
 * Created by hchoang on 22/09/16.
 */

public class DiscussionAdapter extends ArrayAdapter<Discussion> {
    public DiscussionAdapter(Context context, ArrayList<Discussion> arrayList) {
        super(context, 0, arrayList);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Discussion discussion = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_discussion, parent, false);
        }

        TextView discussionName = (TextView) convertView.findViewById(R.id.discussionTitle_textView);
        discussionName.setText(discussion.getTitle());

        TextView authorName = (TextView) convertView.findViewById(R.id.discussionAuthor_textView);
        Reviewer reviewer = discussion.getReviewer();
        authorName.setText(reviewer.getFullName());

        TextView dateTime = (TextView) convertView.findViewById(R.id.discussionDate_textView);
        dateTime.setText(discussion.getDiscussionDate());
        convertView.setTag(position);

        return convertView;
    }
}
