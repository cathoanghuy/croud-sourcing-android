package models.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import hchoang.cs.croudsourcing.R;
import models.Thread;

/**
 * Created by hchoang on 27/09/16.
 */

public class ThreadAdapter extends ArrayAdapter<Thread> {
    public ThreadAdapter(Context context, ArrayList<Thread> threads) {
        super(context, 0, threads);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Thread thread =  (Thread) getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_thread, parent, false);
        }

        TextView threadDescription = (TextView) convertView.findViewById(R.id.threadAuthor_TextView);
        threadDescription.setText(thread.getDescription());

        TextView threadAuthor = (TextView) convertView.findViewById(R.id.authorName_textView);
        threadAuthor.setText(thread.getReviewer().getFullName());

        TextView threadTime = (TextView) convertView.findViewById(R.id.threadDate_TextView);
        threadTime.setText(thread.getThreadDate());

        convertView.setTag(position);

        return convertView;
    }
}
