package models.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import hchoang.cs.croudsourcing.R;
import models.Review;


/**
 * Created by hchoang on 10/10/16.
 */

public class ReviewAdapter extends ArrayAdapter<Review> {
    public ReviewAdapter(Context context, ArrayList<Review> services) {
        super(context, 0, services);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Review review =  getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_review, parent, false);
        }

        TextView title = (TextView) convertView.findViewById(R.id.reviewTitle_TextView);
        TextView description = (TextView) convertView.findViewById(R.id.review_textView);
        TextView reviewer = (TextView) convertView.findViewById(R.id.reviewer_TextView);

        title.setText(review.getTitle());
        description.setText(review.getDescription());
        reviewer.setText(review.getReviewer().getFullName());

        convertView.setTag(review);

        return convertView;
    }
}
