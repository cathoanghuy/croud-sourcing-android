package models.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import hchoang.cs.croudsourcing.R;
import models.Category;
import models.listener.ViewAllServiceListener;

/**
 * Created by hchoang on 6/10/16.
 */

public class CategoryWithRandomServiceAdapter extends ArrayAdapter<Category> {
    public CategoryWithRandomServiceAdapter(Context context, ArrayList<Category> categories) {
        super(context, 0, categories);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Category category = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_category_with_random_service, parent, false);
        }

        TextView categoryName = (TextView) convertView.findViewById(R.id.categoryName_textview);
        categoryName.setText(category.getName());

        TextView viewAllTextView = (TextView) convertView.findViewById(R.id.viewAllService_TextView);
        viewAllTextView.setOnClickListener(new ViewAllServiceListener(category, convertView.getContext()));

        LinearLayout serviceListView = (LinearLayout) convertView.findViewById(R.id.service_listView);
        ServiceAdapter serviceAdapter = new ServiceAdapter(convertView.getContext(), category.getServices());
        serviceListView.removeAllViews();
        int adapterCount = serviceAdapter.getCount();

        for (int i = 0; i < adapterCount; i++) {
            View item = serviceAdapter.getView(i, null, null);
            serviceListView.addView(item);
        }

        convertView.setTag(position);

        return convertView;
    }
}
