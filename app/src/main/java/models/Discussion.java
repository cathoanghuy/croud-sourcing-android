package models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by hchoang on 22/09/16.
 */

public class Discussion implements Serializable {
    private int id;
    private String title;
    private String description;
    private Reviewer reviewer;
    private String discussionDate;

    public Discussion(int id, String title, String description, Reviewer reviewer, String discussionDate) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.reviewer = reviewer;
        this.discussionDate = discussionDate;
    }

    public Discussion(JSONObject discussion) {
        try {
            this.id = discussion.getInt("discussion_id");
            this.title = discussion.getString("discussion_title");
            this.description = discussion.getString("discussion_descrip");
            this.discussionDate = discussion.getString("discussion_date") + " " + discussion.getString("discussion_time");
            this.reviewer = new Reviewer(discussion.getJSONObject("author"));
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public static ArrayList<Discussion> fromJSON(JSONArray jsonArray) {
        ArrayList<Discussion> discussions = new ArrayList<Discussion>();
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                discussions.add(new Discussion(jsonArray.getJSONObject(i)));
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        return discussions;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Reviewer getReviewer() {
        return reviewer;
    }

    public String getDiscussionDate() {
        return discussionDate;
    }
}
