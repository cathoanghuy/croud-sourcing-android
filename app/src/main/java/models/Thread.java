package models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by hchoang on 27/09/16.
 */

public class Thread {
    private Reviewer reviewer;
    private Discussion discussion;
    private String description;
    private String threadDate;
    private boolean status;

    public Thread(Reviewer reviewer, Discussion discussion, String description, String date, boolean status) {
        this.reviewer = reviewer;
        this.discussion = discussion;
        this.description = description;
        this.threadDate = date;
        this.status = status;
    }

    public Thread(JSONObject thread) {
        try {
            this.description = thread.getString("thread_desc");
            int status = thread.getInt("thread_status");
            this.status = (status == 1);
            this.reviewer = new Reviewer(thread.getJSONObject("author"));
            this.threadDate = thread.getString("thread_date") + " " + thread.getString("thread_time");
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public static ArrayList<Thread> fromJSON(JSONArray jsonArray) {
        ArrayList<Thread> threads = new ArrayList<Thread>();
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                threads.add(new Thread(jsonArray.getJSONObject(i)));
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        return threads;
    }

    public Reviewer getReviewer() {
        return reviewer;
    }

    public Discussion getDiscussion() {
        return discussion;
    }

    public String getDescription() {
        return description;
    }

    public String getThreadDate() {
        return threadDate;
    }

    public boolean isStatus() {
        return status;
    }
}
