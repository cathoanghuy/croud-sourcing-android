package layout;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import hchoang.cs.croudsourcing.R;
import hchoang.cs.croudsourcing.hchoang.cs.croudsourcing.ultility.Utility;
import models.Discussion;
import models.Reviewer;
import models.Thread;
import models.adapters.ThreadAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DiscussionFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DiscussionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DiscussionFragment extends BaseFragment {
    private Reviewer reviewer;
    private ArrayList<Thread> threads;
    private ListView threadListView;
    private Discussion discussion;
    private OnFragmentInteractionListener mListener;

    public DiscussionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DiscussionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DiscussionFragment newInstance(String param1, String param2) {
        DiscussionFragment fragment = new DiscussionFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_discussion, container, false);
        reviewer = this.getUser();
        discussion = (Discussion) getActivity().getIntent().getSerializableExtra("Discussion");

        TextView titleTextView = (TextView) view.findViewById(R.id.disTitle_textView);
        TextView descriptionTextView = (TextView) view.findViewById(R.id.disDescription_textView);
        threadListView = (ListView) view.findViewById(R.id.thread_listView);

        titleTextView.setText(discussion.getTitle());
        descriptionTextView.setText(discussion.getDescription());

        Button sendThread = (Button) view.findViewById(R.id.submit_button);
        sendThread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            postDiscussion(baseURL + discussionURL + "/" + discussion.getId() + "/new?api_token=" + getAPIToken(), new RequestParams());
            }
        });

        invokeWS(baseURL + discussionURL + "/" + discussion.getId() + "?api_token=" + getAPIToken(), new RequestParams());

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void attachData() {
        ThreadAdapter categoryAdapter = new ThreadAdapter(getActivity(), threads);
        threadListView.setAdapter(categoryAdapter);
    }

    @Override
    public void invokeWS(String url, RequestParams params) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, params ,new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String response = new String(responseBody, StandardCharsets.UTF_8);
                    JSONObject obj = new JSONObject(response);
                    if(obj.getInt("code") == 200) {
                        threads = Thread.fromJSON(obj.getJSONArray("threads"));
                        attachData();
                    } else{
                        Toast.makeText(getActivity().getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getActivity().getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if(statusCode == 404){
                    Toast.makeText(getActivity().getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                } else if(statusCode == 500){
                    Toast.makeText(getActivity().getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                } else{
                    Toast.makeText(getActivity().getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void postDiscussion(String url, RequestParams params) {
        Map<String, String> thread = new HashMap<>();

        EditText threadEditText = (EditText) getActivity().findViewById(R.id.thread_editText);

        if (!Utility.isNotNull(threadEditText.getText().toString())) {
            Toast.makeText(getActivity().getApplicationContext(), "Content is Empty, Please Enter", Toast.LENGTH_LONG).show();
            return;
        }

        thread.put("thread_author", Integer.toString(reviewer.getId()));
        thread.put("thread_discussion_id", Integer.toString(discussion.getId()));
        thread.put("thread_desc", threadEditText.getText().toString());

        params.put("new_thread", thread);

        AsyncHttpClient client = new AsyncHttpClient();
        client.post(url, params ,new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String response = new String(responseBody, StandardCharsets.UTF_8);
                    JSONObject obj = new JSONObject(response);
                    Toast.makeText(getActivity().getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                    getActivity().finish();
                    startActivity(getActivity().getIntent());
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getActivity().getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if(statusCode == 404){
                    Toast.makeText(getActivity().getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                } else if(statusCode == 500) {
                    Toast.makeText(getActivity().getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                } else{
                    Toast.makeText(getActivity().getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
