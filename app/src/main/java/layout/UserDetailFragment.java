package layout;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import hchoang.cs.croudsourcing.R;
import hchoang.cs.croudsourcing.hchoang.cs.croudsourcing.ultility.Utility;
import models.Reviewer;
import models.listener.CloseCurrentActivityListener;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UserDetailFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UserDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserDetailFragment extends BaseFragment {

    private OnFragmentInteractionListener mListener;
    EditText firstName;
    EditText lastName;
    EditText email;
    EditText password;

    public UserDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment UserDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UserDetailFragment newInstance() {
        UserDetailFragment fragment = new UserDetailFragment();
        return fragment;
    }

    @Override
    public void invokeWS(String url, RequestParams params) {
//
    }

    private void attachData(Reviewer reviewer) {
        firstName.setText(reviewer.getFirstName());
        lastName.setText(reviewer.getLastName());
        email.setText(reviewer.getEmail());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_detail, container, false);
        Reviewer reviewer = getUser();
        Button backButton = (Button) view.findViewById(R.id.back_Button);
        backButton.setOnClickListener(new CloseCurrentActivityListener(this.getActivity()));
        firstName = (EditText) view.findViewById(R.id.firstname_editText);
        lastName = (EditText) view.findViewById(R.id.lastname_editText);
        email = (EditText) view.findViewById(R.id.email_editText);
        password = (EditText) view.findViewById(R.id.discussionDescription_editText);

        Button updateButton = (Button) view.findViewById(R.id.update_Button);

        updateButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final Reviewer reviewer = getUser();
                reviewer.setFirstName(firstName.getText().toString());
                reviewer.setLastName(lastName.getText().toString());
                Map<String, String> updateInfoParam = new HashMap<>();
                if (Utility.isNotNull(firstName.getText().toString()) && Utility.isNotNull(lastName.getText().toString())) {
                    updateInfoParam.put("reviewer_first_name", reviewer.getFirstName());
                    updateInfoParam.put("reviewer_last_name", reviewer.getLastName());
                    if (Utility.isNotNull(password.getText().toString())) {
                        updateInfoParam.put("reviewer_password", password.getText().toString());
                    }
                    AsyncHttpClient client = new AsyncHttpClient();
                    RequestParams params = new RequestParams();
                    params.put("update_detail", updateInfoParam);
                    client.post(baseURL + updateProfileURL + "?api_token=" + getAPIToken(), params, new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            try {
                                String response = new String(responseBody, StandardCharsets.UTF_8);
                                JSONObject obj = new JSONObject(response);
                                if (obj.getInt("code") == 200) {
                                    Toast.makeText(getActivity().getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                                    storeUser(reviewer);
                                } else {
                                    Toast.makeText(getActivity().getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                Toast.makeText(getActivity().getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                                e.printStackTrace();

                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            if (statusCode == 404) {
                                Toast.makeText(getActivity().getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                            } else if (statusCode == 500) {
                                Toast.makeText(getActivity().getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getActivity().getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        });
        attachData(reviewer);
//        invokeWS(baseURL + profileURL + "?api_token=" + getAPIToken(), new RequestParams());
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
