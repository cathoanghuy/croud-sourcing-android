package layout;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import hchoang.cs.croudsourcing.GenericActivity;
import hchoang.cs.croudsourcing.R;
import hchoang.cs.croudsourcing.hchoang.cs.croudsourcing.ultility.Utility;
import models.Service;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link WriteReviewFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link WriteReviewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WriteReviewFragment extends BaseFragment {

    private Service service;
    RatingBar overallRating;
    RatingBar moneyValueRating;
    RatingBar easeOfUseRating;
    RatingBar featuresRating;
    RatingBar customerSupportRating;
    EditText reviewEditText;
    EditText reviewTitleEditText;
    EditText prooOfPurchase;
    private OnFragmentInteractionListener mListener;

    public WriteReviewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment WriteReviewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static WriteReviewFragment newInstance(Service service) {
        WriteReviewFragment fragment = new WriteReviewFragment();
        fragment.setService(service);
        return fragment;
    }

    @Override
    public void invokeWS(String url, RequestParams params) {
        Log.i("post review", "invokeWS: " + url);
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(url, params ,new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String response = new String(responseBody, StandardCharsets.UTF_8);
                    JSONObject obj = new JSONObject(response);
                    Toast.makeText(getActivity().getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                    getActivity().finish();
                    Intent intent = new Intent(getActivity(), GenericActivity.class);
                    intent.putExtra("layout", getString(R.string.service_detail_layout));
                    intent.putExtra("service", service);
                    startActivity(intent);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getActivity().getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if(statusCode == 404){
                    Toast.makeText(getActivity().getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                } else if(statusCode == 500){
                    Toast.makeText(getActivity().getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_write_review, container, false);
        TextView serviceName = (TextView) view.findViewById(R.id.serviceName_TextView);
        serviceName.setText("Write a Review for " + service.getName());

        overallRating = (RatingBar) view.findViewById(R.id.overallReview_reviewBar);
        moneyValueRating = (RatingBar) view.findViewById(R.id.valueForMoney_reviewBar);
        easeOfUseRating = (RatingBar) view.findViewById(R.id.easeOfUse_ratingBar);
        featuresRating = (RatingBar) view.findViewById(R.id.feature_ratingBar);
        customerSupportRating = (RatingBar) view.findViewById(R.id.customerSupport_ratingBar);
        reviewEditText = (EditText) view.findViewById(R.id.review_editText);
        reviewTitleEditText = (EditText) view.findViewById(R.id.reviewTitle_editText);
        prooOfPurchase = (EditText) view.findViewById(R.id.proofOfPurchase_editText);


        Button submitButton = (Button) view.findViewById(R.id.submit_button);
        submitButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!Utility.isNotNull(reviewTitleEditText.getText().toString())) {
                    Toast.makeText(getActivity().getApplicationContext(), "Please enter your review title", Toast.LENGTH_LONG).show();
                    return;
                }
                if (!service.isServiceTrial() && !Utility.isNotNull(prooOfPurchase.getText().toString())) {
                    Toast.makeText(getActivity().getApplicationContext(), "This service do not provide trial service, please enter your Invoice Number before continue", Toast.LENGTH_LONG).show();
                    return;
                }
                RequestParams params = new RequestParams();
                Map<String, String> review = new HashMap<>();
                review.put("review_ratings", Float.toString(overallRating.getRating()));
                review.put("review_value_for_money", Float.toString(moneyValueRating.getRating()));
                review.put("review_ease_of_use", Float.toString(easeOfUseRating.getRating()));
                review.put("review_feature", Float.toString(featuresRating.getRating()));
                review.put("review_customer_support", Float.toString(customerSupportRating.getRating()));
                review.put("review_serv_id", Integer.toString(service.getId()));
                review.put("review_reviewer", Integer.toString(getUser().getId()));
                review.put("review_title", reviewTitleEditText.getText().toString());
                review.put("review_desc", reviewEditText.getText().toString());
                review.put("review_invoice_no", prooOfPurchase.getText().toString());

                params.put("new_review", review);

                invokeWS(baseURL + newReviewURL + "?api_token=" + getAPIToken(), params);
            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void setService(Service service) {
        this.service = service;
    }
}
