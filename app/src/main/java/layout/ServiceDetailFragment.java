package layout;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import hchoang.cs.croudsourcing.GenericActivity;
import hchoang.cs.croudsourcing.R;
import models.Review;
import models.Service;
import models.adapters.ReviewAdapter;
import models.listener.OpenWebViewListener;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ServiceDetailFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ServiceDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ServiceDetailFragment extends BaseFragment {

    private OnFragmentInteractionListener mListener;
    private Service service;
    private ArrayList<Review> reviews;
    private LinearLayout reviewListView;

    public ServiceDetailFragment() {
        // Required empty public constructor
    }

    public static ServiceDetailFragment newInstance() {
        ServiceDetailFragment fragment = new ServiceDetailFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public static ServiceDetailFragment newInstance(Service service) {
        ServiceDetailFragment fragment = new ServiceDetailFragment();
        fragment.setService(service);
        return fragment;
    }

    @Override
    public void invokeWS(String url, RequestParams params) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, params ,new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String response = new String(responseBody, StandardCharsets.UTF_8);
                    JSONObject obj = new JSONObject(response);
                    if(obj.getInt("code") == 200) {
                        service = new Service(obj.getJSONObject("service"));
                        reviews = Review.fromJSON(obj.getJSONArray("reviews"));
                        setView();
                        attachData();
                    } else{
                        Toast.makeText(getActivity().getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getActivity().getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if(statusCode == 404){
                    Toast.makeText(getActivity().getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                } else if(statusCode == 500){
                    Toast.makeText(getActivity().getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                } else{
                    Toast.makeText(getActivity().getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_service_detail, container, false);
        reviewListView = (LinearLayout) view.findViewById(R.id.review_listView);
        String apiToken = getAPIToken();

        invokeWS(baseURL + servicesURL + "/" + service.getId()  + "/" + reviewURL  + "?api_token=" + apiToken, new RequestParams());

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void setService(Service service) {
        this.service = service;
    }

    private void setView() {
        TextView serviceName = (TextView) getActivity().findViewById(R.id.reviewTitle_TextView);
        TextView serviceDescription = (TextView) getActivity().findViewById(R.id.serviceDescription_textView);
        TextView servicePrice = (TextView) getActivity().findViewById(R.id.price_TextView);
        TextView serviceLink = (TextView) getActivity().findViewById(R.id.goToApp_TextView);
        ImageView serviceTrial = (ImageView) getActivity().findViewById(R.id.serviceTrial_ImageView);
        ImageView mobileApp = (ImageView) getActivity().findViewById(R.id.mobile_ImageView);
        Button postReviewButton = (Button) getActivity().findViewById(R.id.postReview_Button);
        RatingBar ratingBar = (RatingBar) getActivity().findViewById(R.id.serviceRating_ratingBar);

        serviceName.setText(service.getName());
        serviceDescription.setText(Html.fromHtml(service.getDescription()));

        if (service.isServiceTrial()) {
            serviceTrial.setImageResource(R.drawable.tick);
        } else {
            serviceTrial.setImageResource(R.drawable.cross);
        }

        if (service.hasMobileApp()) {
            mobileApp.setImageResource(R.drawable.tick);
        } else {
            mobileApp.setImageResource(R.drawable.cross);
        }

        if (!service.getServiceLink().isEmpty()) {
            serviceLink.setOnClickListener(new OpenWebViewListener(this.getActivity(), service.getServiceLink()));
        } else {
            serviceLink.setText("N/A");
        }

        ratingBar.setRating((float) service.getEditorRatings());

        servicePrice.setText("$" + Double.toString(service.getPrice()));

        if (!service.isReviewable()) {
            postReviewButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Toast.makeText(getActivity().getApplicationContext(), "You already post a review for this service", Toast.LENGTH_LONG).show();
                }
            });
        } else {
            postReviewButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), GenericActivity.class);
                    intent.putExtra("layout", getString(R.string.write_review_layout));
                    intent.putExtra("service", service);
                    startActivity(intent);
                }
            });
        }


    }

    private void attachData() {
        ReviewAdapter reviewAdapter = new ReviewAdapter(getActivity(), reviews);
        final int adapterCount = reviewAdapter.getCount();

        for (int i = 0; i < adapterCount; i++) {
            View item = reviewAdapter.getView(i, null, null);
            reviewListView.addView(item);
        }
    }
}
