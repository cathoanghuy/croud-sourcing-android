package layout;



import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import hchoang.cs.croudsourcing.R;
import models.Review;
import models.Reviewer;

/**
 * Created by hchoang on 19/09/16.
 */
public abstract class BaseFragment extends Fragment {

    public abstract void invokeWS(String url, RequestParams params);
    protected static SharedPreferences sharedPreferences;
    protected String baseURL;
    protected String loginURL;
    protected String discussionURL;
    protected String categoryURL;
    protected String servicesURL;
    protected String exploreURL;
    protected String reviewURL;
    protected String profileURL;
    protected String updateProfileURL;
    protected String randomServiceURL;
    protected String newDiscussionURL;
    protected String newReviewURL;
    protected String registerURL;

    public String getAPIToken() {
        return sharedPreferences.getString("api_token", null);
    }

    public Reviewer getUser() {
        Gson gson = new Gson();
        String json = sharedPreferences.getString("user", null);
        Reviewer reviewer = gson.fromJson(json, Reviewer.class);
        return reviewer;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        baseURL = getContext().getString(R.string.BASE_URL);
        discussionURL = getContext().getString(R.string.DISCUSSION);
        categoryURL = getContext().getString(R.string.CATEGORY);
        servicesURL = getContext().getString(R.string.SERVICE);
        loginURL = getContext().getString(R.string.LOGIN_URL);
        exploreURL = getContext().getString(R.string.EXPLORE);
        reviewURL = getString(R.string.REVIEW);
        profileURL = getString(R.string.PROFILE);
        updateProfileURL = getString(R.string.UPDATE_PROFILE);
        randomServiceURL = getString(R.string.RANDOM_SERVICE);
        newDiscussionURL = getString(R.string.NEW_DISCUSSION);
        newReviewURL = getString(R.string.NEW_REVIEW);
        registerURL = getString(R.string.REGISTER_URL);

        String ref = getContext().getString(R.string.MY_PREF);

        sharedPreferences = getActivity().getSharedPreferences(ref, Context.MODE_PRIVATE);
        super.onCreate(savedInstanceState);
    }


    public void storeUser(Reviewer reviewer) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(reviewer);
        editor.putString("user", json).apply();
    }
}
