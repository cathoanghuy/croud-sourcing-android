package layout;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import hchoang.cs.croudsourcing.R;
import hchoang.cs.croudsourcing.hchoang.cs.croudsourcing.ultility.Utility;

/**
 * Activities that contain this fragment must implement the
 * {@link NewUserFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NewUserFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NewUserFragment extends BaseFragment {

    private OnFragmentInteractionListener mListener;
    private TextView firstNameEditText;
    private TextView lastNameEditText;
    private TextView emailEditText;
    private TextView positionEditText;
    private TextView passwordEditText;
    private TextView repeatPasswordEditText;
    private TextView invitationCodeEditText;
    private Button backButton;


    public NewUserFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment NewUserFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NewUserFragment newInstance() {
        NewUserFragment fragment = new NewUserFragment();
        return fragment;
    }

    @Override
    public void invokeWS(String url, RequestParams params) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_new_user, container, false);
        backButton = (Button) view.findViewById(R.id.back_Button);

        firstNameEditText = (TextView) view.findViewById(R.id.firstname_editText);
        lastNameEditText = (TextView) view.findViewById(R.id.lastname_editText);
        emailEditText = (TextView) view.findViewById(R.id.email_editText);
        positionEditText = (TextView) view.findViewById(R.id.position_editText);
        passwordEditText = (TextView) view.findViewById(R.id.discussionDescription_editText);
        repeatPasswordEditText = (TextView) view.findViewById(R.id.repeatPassword_editText);
        invitationCodeEditText = (TextView) view.findViewById(R.id.invitation_editText);

        backButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                openLoginFragment();
            }
        });

        Button newUserButton = (Button) view.findViewById(R.id.register_Button);
        newUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void register() {
        Map<String, String> newUser = new HashMap<String, String>();
        if (!Utility.isNotNull(firstNameEditText.getText().toString())) {
            Toast.makeText(getActivity().getApplicationContext(), "First Name field is required", Toast.LENGTH_LONG).show();
        } else if (!Utility.isNotNull(lastNameEditText.getText().toString())) {
            Toast.makeText(getActivity().getApplicationContext(), "Last Name field is required", Toast.LENGTH_LONG).show();
        } else if (!Utility.isNotNull(emailEditText.getText().toString())) {
            Toast.makeText(getActivity().getApplicationContext(), "Email field is required", Toast.LENGTH_LONG).show();
        } else if (!Utility.isNotNull(passwordEditText.getText().toString())) {
            Toast.makeText(getActivity().getApplicationContext(), "Password field is required", Toast.LENGTH_LONG).show();
        } else if (!Utility.isNotNull(repeatPasswordEditText.getText().toString())) {
            Toast.makeText(getActivity().getApplicationContext(), "Repeat Password field is required", Toast.LENGTH_LONG).show();
        } else if (!Utility.isNotNull(invitationCodeEditText.getText().toString())) {
            Toast.makeText(getActivity().getApplicationContext(), "Invitation Code field is required", Toast.LENGTH_LONG).show();
        } else if (!Utility.isNotNull(positionEditText.getText().toString())) {
            Toast.makeText(getActivity().getApplicationContext(), "Position field is required", Toast.LENGTH_LONG).show();
        } else if (!Utility.validateEmail(emailEditText.getText().toString())) {
            Toast.makeText(getActivity().getApplicationContext(), "Invalid Email Format", Toast.LENGTH_LONG).show();
        } else if (!passwordEditText.getText().toString().equals(repeatPasswordEditText.getText().toString())) {
            Toast.makeText(getActivity().getApplicationContext(), "Password and Repeat Password must be identical", Toast.LENGTH_LONG).show();
        } else {
            newUser.put("reviewer_first_name", firstNameEditText.getText().toString());
            newUser.put("reviewer_last_name", lastNameEditText.getText().toString());
            newUser.put("reviewer_password", passwordEditText.getText().toString());
            newUser.put("reviewer_email", emailEditText.getText().toString());
            newUser.put("reviewer_code", invitationCodeEditText.getText().toString());
            newUser.put("reviewer_position", positionEditText.getText().toString());

            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            params.put("new_user", newUser);
            client.post(baseURL + registerURL, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    try {
                        String response = new String(responseBody, StandardCharsets.UTF_8);
                        JSONObject obj = new JSONObject(response);
                        if (obj.getInt("code") == 200) {
                            openLoginFragment();
                        }
                        Toast.makeText(getActivity().getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        Toast.makeText(getActivity().getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    if (statusCode == 404) {
                        Toast.makeText(getActivity().getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                    } else if (statusCode == 500) {
                        Toast.makeText(getActivity().getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity().getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }

    private void openLoginFragment() {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        LoginFragment fragment = LoginFragment.newInstance();
        fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).commit();
    }
}
