package layout;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import hchoang.cs.croudsourcing.AppActivity;
import hchoang.cs.croudsourcing.R;
import hchoang.cs.croudsourcing.hchoang.cs.croudsourcing.ultility.Utility;

/**
 * Activities that contain this fragment must implement the
 * {@link NewDiscussionFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NewDiscussionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NewDiscussionFragment extends BaseFragment {

    private OnFragmentInteractionListener mListener;
    private EditText discussionTitleEditText;
    private EditText discussionDescriptionEditText;

    public NewDiscussionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment NewDiscussionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NewDiscussionFragment newInstance() {
        NewDiscussionFragment fragment = new NewDiscussionFragment();
        return fragment;
    }

    @Override
    public void invokeWS(String url, RequestParams params) {
        Map<String, String> newDiscussion = new HashMap<>();
        if (!Utility.isNotNull(discussionTitleEditText.getText().toString()) ||
                !Utility.isNotNull(discussionDescriptionEditText.getText().toString())) {
            Toast.makeText(getActivity().getApplicationContext(), "Fields must not be empty, please enter", Toast.LENGTH_LONG).show();
            return;
        }
        newDiscussion.put("discussion_title", discussionTitleEditText.getText().toString());
        newDiscussion.put("discussion_descrip", discussionDescriptionEditText.getText().toString());
        newDiscussion.put("author_id", Integer.toString(getUser().getId()));

        params.put("new_discussion", newDiscussion);
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(url, params ,new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String response = new String(responseBody, StandardCharsets.UTF_8);
                    JSONObject obj = new JSONObject(response);
                    Toast.makeText(getActivity().getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getActivity(), AppActivity.class);
                    intent.putExtra("tab_position", 2);
                    startActivity(intent);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getActivity().getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if(statusCode == 404){
                    Toast.makeText(getActivity().getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                } else if(statusCode == 500){
                    Toast.makeText(getActivity().getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_discussion, container, false);
        Button postButton = (Button) view.findViewById(R.id.post_button);
        discussionTitleEditText = (EditText) view.findViewById(R.id.descriptionTitle_editText);
        discussionDescriptionEditText = (EditText) view.findViewById(R.id.discussionDescription_editText);

        postButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                invokeWS(baseURL + newDiscussionURL + "?api_token=" + getAPIToken(), new RequestParams());
            }
        });

        Button backButton = (Button) view.findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

//        invokeWS(baseURL + newDiscussionURL + "?api_token=" + getAPIToken(), new RequestParams());

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
